package hieutm;

import java.util.Objects;

public class KinematicSeek {
	private Kinematic character;
	private Kinematic target;
	private float maxSpeed;
	
	public KinematicSeek() {
		
	}
	
	public KinematicSeek(Kinematic character, Kinematic target, float maxSpeed) {
		this.character = character;
		this.target = target;
		this.maxSpeed = maxSpeed;
	}

	public Kinematic getCharacter() {
		return character;
	}

	public void setCharacter(Kinematic character) {
		this.character = character;
	}

	public Kinematic getTarget() {
		return target;
	}

	public void setTarget(Kinematic target) {
		this.target = target;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final KinematicSeek other = (KinematicSeek) obj;
		if (Float.floatToIntBits(this.maxSpeed) != Float.floatToIntBits(other.maxSpeed)) {
			return false;
		}
		if (!Objects.equals(this.character, other.character)) {
			return false;
		}
		if (!Objects.equals(this.target, other.target)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "KinematicSeek{" + "charater=" + character + ", target=" + target + ", maxSpeed=" + maxSpeed + '}';
	}

	public KinematicOutput getStering() {
		Vector2D velocity = new Vector2D();
		velocity = character.getPosition().sub(target.getPosition());
		velocity.normalize();
		velocity.multi(maxSpeed);
		return new KinematicOutput(velocity, 0);
	}
}
