package hieutm;

import java.util.Objects;

public class Kinematic {
	private Vector2D position;
	private float orientation;
	private Vector2D velocity;
	private float rotation;
	
	public Kinematic() {
		
	}
	
	public Kinematic(Vector2D position, float orientation, Vector2D velocity, float rotation) {
		this.position = position;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
	}

	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public float getOrientation() {
		return orientation;
	}

	public void setOrientation(float orientation) {
		this.orientation = orientation;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public float getRotation() {
		return rotation;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if(getClass() != obj.getClass()){
			return false;
		}
		final Kinematic other = (Kinematic) obj;
		if (!Objects.equals(this.position, other.position)) {
			return false;
		}
		if (Float.floatToIntBits(this.orientation) != Float.floatToIntBits(other.orientation)) {
			return false;
		}
		if (!Objects.equals(this.velocity, other.velocity)) {
			return false;
		}
		if (Float.floatToIntBits(this.rotation) != Float.floatToIntBits(other.rotation)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Character{" + "position=" + position + ", orientation=" + orientation + ", velocity=" + velocity + ", rotation=" + rotation + '}';
	}

	public void update(KinematicOutput kine, float time) {
		this.velocity = kine.getVelocity();
		this.rotation = kine.getRotation();

		this.position.add(this.velocity.multi(time));
		this.orientation += this.rotation*time;
	}

	public void applyNewOrientation(){
		if(this.velocity.length()>0){
			this.orientation = (float)Math.atan2(-this.velocity.getX(), this.rotation);
		}
	}

	public static void main(String[] args){

	}
}
