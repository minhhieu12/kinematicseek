package hieutm;

public class Vector2D {
	private float x;
	private float z;

	public Vector2D() {	}

	public Vector2D(float x, float z) {
		this.x = x;
		this.z = z;
	}

	@Override
	public String toString(){
		return "Vector2D{" + "X=" + x + ", Z=" + z + '}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2D other = (Vector2D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}
	
	public Vector2D add(Vector2D a) {
		this.setX(this.getX()+a.getX());
		this.setZ(this.getZ()+a.getZ());
		return this;
	}
	
	public Vector2D add(Vector2D a, Vector2D b) {
		return new Vector2D(b.getX() + a.getX(), b.getZ() + a.getZ());
	}

	public Vector2D sub(Vector2D a, Vector2D b){
		return new Vector2D(a.getX()-b.getX(), a.getZ()-b.getZ());
	}

	public Vector2D sub(Vector2D a){
		this.setX(this.getX()-a.getX());
		this.setZ(this.getZ()-a.getZ());
		return this;
	}

	public Vector2D multi(Vector2D a, float c){
		return new Vector2D(a.getX()*c, a.getZ()*c);
	}

	public Vector2D multi(float c){
		this.setX(this.getX()*c);
		this.setZ(this.getX()*c);
		return this;
	}

	public double length(){
		return Math.sqrt(this.x*this.x + this.z*this.z);
	}

	public Vector2D normalize(){
		double length = this.length();
		this.x /= length();
		this.z /= length();
		return this;
	}

	public static void main(String[] args) {
		Vector2D a = new Vector2D(2,3);
		Vector2D b = new Vector2D(2,3);
		System.out.println(a.add(b).toString());
		System.out.println(a.sub(b).toString());
		System.out.println(a.multi(4).toString());
		System.out.println(a.length());
		System.out.println(a.normalize().toString());
	}
}
